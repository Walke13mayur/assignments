/*
 * Main.cpp
 *
 *  Created on: 29-Jul-2020
 *      Author: mayur
 */
#include<iostream>
#include<string>
using namespace std;

class Account
{
private:
	char name[30];
	int accNo;
	char accType[30];
	float balance;
public:
	void accept_acc_info()
	{
		cout<<"Enter name	:	";
		cin>>this->name;
		cout<<"Enter accNO	:	";
		cin>>this->accNo;
		cout<<"Enter accType	:	";
		cin>>this->accType;
		cout<<"Enter balance	:	";
		cin>>this->balance;
	}
	void print_acc_info()
	{
		cout<<"Name	:	"<<this->name<<endl;
		cout<<"accNo	:	"<<this->accNo<<endl;
		cout<<"accType	:	"<<this->accType<<endl;
		cout<<"Balance	:	"<<this->balance<<endl;
	}
	friend class Bank;
};

class Bank
{
private:
	Account arr[3];
public:
	void create_accounts(Bank *ptr, int *index)
	{
		ptr->arr[*index].accept_acc_info();
		printf("Account has been created successfully :)\n");
	}
	void deposit(int accNo, float amount, Bank *ptr)
	{
		for(int index = 0; index < 5; index++)
		{
			if(ptr->arr[index].accNo == accNo)
			{
				ptr->arr[index].balance += amount;
			}
		}
	}
	void withDraw(int accNo, float amount, Bank *ptr)
	{
		for(int index = 0; index < 5; index++)
		{
			if(ptr->arr[index].accNo == accNo)
			{
				ptr->arr[index].balance -= amount;
			}
		}
	}
	void print_acc_details(int accNo, Bank *ptr)
	{
		for(int index = 0; index < 3; index++)
			{
				if(ptr->arr[index].accNo == accNo)
				{
					printf("Name	:	%s\n",ptr->arr[index].name);
					printf("accNo	:	%d\n",ptr->arr[index].accNo);
					printf("accType	:	%s\n",ptr->arr[index].accType);
					printf("balance	:	%f\n",ptr->arr[index].balance);
				}
			}
	}
	void print_all_accounts(Bank *ptr, int *index)
	{
		ptr->arr[*index].print_acc_info();
	}
};

int menu_list()
{
	int choice;
	printf("0.Exit\n");
	printf("1.Create Accounts\n");
	printf("2.Deposit amount\n");
	printf("3.Withdraw amount\n");
	printf("4.Check current details of your account\n");
	printf("5.Print all accounts\n");
	printf("Enter choice	:	");
	scanf("%d",&choice);
	return choice;
}
int main(void)
{
	int choice;
	while((choice = ::menu_list()) != 0)
	{
		int accNo;
		float amount;
		Bank bank;
		switch(choice)
		{
		case 1:
			//create accounts
			for(int index = 0; index < 3; index++)
			{
				bank.create_accounts(&bank, &index);
			}
			break;
		case 2:
			//deposit
			printf("Account no	:	");
			scanf("%d",&accNo);
			printf("Enter amount to deposit	:	");
			scanf("%f",&amount);
			bank.deposit(accNo, amount, &bank);
			break;
		case 3:
			//withdraw
			printf("Account no	:	");
			scanf("%d",&accNo);
			printf("Enter amount to withdraw	:	");
			scanf("%f",&amount);
			bank.withDraw(accNo, amount, &bank);
			break;
		case 4:
			//print acc by acc no
			printf("Account no	:	");
			scanf("%d",&accNo);
			bank.print_acc_details(accNo, &bank);
			break;
		case 5:
			//print all acc
			for(int index = 0; index < 3; index++)
			{
				bank.print_all_accounts(&bank, &index);
			}
			break;
		}
	}
	return 0;
}
