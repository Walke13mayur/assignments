/*
 * main.cpp
 *
 *  Created on: 4-aug-2020
 *      Author: mayur
 */

#include <iostream>
#include <string>
#include <vector>
using namespace std;
class IllegalAmount
{
private:
	string message;
public:
	IllegalAmount( const string message ) throw( ) : message( message )
	{	}
	string getMessage( void )const throw( )
	{
		return this->message;
	}
};
class IllegalAccount
{
private:
	string message;
public:
	IllegalAccount( const string message ) throw( ) : message( message )
	{	}
	string getMessage( void )const throw( )
	{
		return this->message;
	}
};
class Account
{
private:
	int acc_Num;
	string name;
	string type;
	float balance;
public:

    Account();
    void setAccount();
    void setName(string name);
    void setType(string type);
    void addBalance(float amt)throw(IllegalAmount);
    void subBalance(float amt)throw(IllegalAmount);
	int getAccNum()const;
	string getName()const;
	string getType()const;
	float getBanlance()const;

};
Account :: Account ()
{
	this->acc_Num = 1000;
	this->balance = 0.0;
}
void Account ::setAccount()
{
	this->acc_Num += 1;
}
void Account :: setName(string name)
{
	this->name = name;
}
void Account :: setType(string type)
{
	this->type = type;
}
void Account :: addBalance(float amt)throw(IllegalAmount)
{
	if(amt<0)
		throw IllegalAmount("Amount less than zero not allowed");
	this->balance += amt;
}
void Account :: subBalance(float amt)throw(IllegalAmount)
{
	if(amt<0)
		throw IllegalAmount("Amount less than zero not allowed");
	this->balance -= amt;
}
int Account :: getAccNum()const
{
	return this->acc_Num;
}
string Account :: getName()const
{
	return this->name;
}
string Account :: getType()const
{
	return this->type;
}
float Account ::getBanlance()const
{
	return this->balance;
}

class Bank
{
private:
	int index;
	vector <Account> arr;
public:

	Bank();
	void setIndex();
	int getIndex();
	vector<Account>* getVector();
	void createAccount(Account acc);

	void printDetails(int num);

	float deposit(int num, float amt);

	float widthdrwal(int num, float amt);
};
 Bank :: Bank()
{
	this->index = -1;
}
 void Bank :: setIndex()
 {
	 this->index += 1;
 }
 int Bank :: getIndex()
 {
	 return this->index;
 }
 vector<Account>* Bank :: getVector()
 {
	 return &this->arr;
 }
void Bank :: createAccount(Account acc)
{
	this->index +=1;
	this->arr.push_back(acc);

	cout<<"Account has been created : "<<this->arr[index].getAccNum()<<endl;

}

void Bank :: printDetails(int num)
{
	for(int i=0;i<=this->index;i++)
	{
		if(this->arr[i].getAccNum() == num)
		{
			cout<<"Name     : "<<this->arr[i].getName()<<endl;
			cout<<"Number   : "<<this->arr[i].getAccNum()<<endl;
			cout<<"Type     : "<<this->arr[i].getType()<<endl;
			cout<<"Balance  : "<<this->arr[i].getBanlance()<<endl;
			break;
		}
	}

}
float Bank :: deposit(int num, float amt)
{
	int i;
	for( i=0;i<=this->index;i++)
	{
		if(this->arr[i].getAccNum() == num){
			this->arr[i].addBalance(amt);
			break;
		}
	}
	return this->arr[i].getBanlance();

}
float Bank :: widthdrwal(int num, float amt)
{
	int i;
	for( i=0;i<=index;i++)
	{
		if(this->arr[i].getAccNum() == num){
			this->arr[i].subBalance(amt);
		    break;
		}
	}
	return this->arr[i].getBanlance();

}
int menu_list( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Create New Account"<<endl;
	cout<<"2.Deposit"<<endl;
	cout<<"3.Withdraw"<<endl;
	cout<<"4.Print Account details"<<endl;
	cout<<"Enter choice : "<<endl;
	cin>>choice;
	return choice;
}
int accept_account_number( )throw(IllegalAccount)
{
	int number;
	cout<<"Account number : "<<endl;
	cin>>number;
	if(number<1000)
		throw IllegalAccount("Account not accepted");
	return number;
}
float accept_amount( )throw(IllegalAmount)
{
	float amount;
	cout<<"Amount : "<<endl;
	cin>>amount;
	if(amount<0)
		throw IllegalAmount("Amount not accepted");
	return amount;
}
void print_balance( float balance )
{
	cout<<"Balance : "<<balance<<endl;
}
void acceptRecord(Account &acc)
{
	float balance;
	string name,type;
	acc.setAccount();
	cout<<"Name      : ";
	cin>>name;
	acc.setName(name);
	cout<<"Type      : ";
	cin>>type;
	acc.setType(type);
	cout<<"Balance   : ";
	cin>>balance;
	acc.addBalance(balance);
}
int main()
{
	int choice, accNumber;
	float amount,balance;
	Account acc;
	Bank bank;
	while( ( choice = menu_list( ) ) )
	{
		try
		{
		switch( choice )
		{
		case 1:
			acceptRecord(acc);
			bank.createAccount(acc);

		case 2:
			accNumber = accept_account_number();
			amount = accept_amount(  );
			balance = bank.deposit(accNumber, amount );
			print_balance(balance);
			break;

		case 3:
			accNumber = accept_account_number();
			amount = accept_amount(  );
			balance = bank.widthdrwal( accNumber, amount );
			print_balance(balance);
			break;

		case 4:
			accNumber = accept_account_number();
			bank.printDetails(accNumber);
		}
		}
		catch(IllegalAccount &ex)
		{
			ex.getMessage();
		}
		catch(IllegalAmount &ex)
		{
			cout<<ex.getMessage()<<endl;
		}
	}
	return 0;
}


