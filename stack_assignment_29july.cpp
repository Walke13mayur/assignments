//============================================================================
// Name        : stack.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//===========================================================================

	
#include <bits/stdc++.h>
using namespace std;
#define MAX 4
class Stack
{
	int top;
	int a[MAX]; // Maximum size of Stack

public:
	Stack( )
	{
		top = -1;
	}
	void push( int element)
	{
		if( top >= MAX - 1 )
		{
			cout << "Stack Overflow" << endl;
		}
		else
		{
			top++;
			this->a[ top ] = element;
		}
	}

	void getElement(  )
	{
		if( top >= 0)
		{
			cout<<"Stack Elements Are:";
			for(int i=top; i>=0; i--)
				cout<<a[i]<<" ";
			cout<<endl;
		}
		else
			cout<<"Stack is empty";
	}

	int peek( )
	{
		return this->a[ top ];
	}

	void pop( )
	{
		if( top <= -1 )
			cout <<"Stack is Empty"<<endl;
		else
		{
			cout <<"Popped Element : " <<  a[ top ];
			top--;
		}
	}
};

int menu_list( void )
{
	int choice ;
	cout << "\n0:Exit" << endl;
	cout << "1:Push Element In Stack:" << endl;
	cout << "2:Peek Element From Stack:" << endl;
	cout << "3:Pop Element From Stack:" << endl;
	cout << "4:Display Elements Of Stack:" << endl;
	cout << "Enter the choice:	" << endl;
	cin  >> choice;
	return choice;
}

int main( void )
{
	Stack s;
	int element , choice ;
	while( ( choice = ::menu_list() ) != 0 )
	{
		switch (choice)
		{
		case 1:
			cout << "Enter Element: ";
			cin >> element ;
			s.push( element );
			break;

		case 2:
			int peek;
			peek = s.peek( );
			cout <<"Top Element : " << peek ;
			break;

		case 3:
			s.pop();
			break;

		case 4:
			s.getElement( );
			break;
		}
	}

	return 0;
}
