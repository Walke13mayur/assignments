//============================================================================
// Name        : day7 question 2
// Author      : mayyur
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

class Date{
	int day;
	int month;
	int year;
public:
	void init()
	{
		this->day = 1;
		this->month = 1;
		this->year = 1;
	}

	void acceptRecord()
	{
		cout<<"Enter the day:	"<<endl;
		cin>>this->day;
		cout<<"Enter the Month:	"<<endl;
		cin>>this->month;
		cout<<"Enter the Year:	"<<endl;
		cin>>this->year;
	}
	void printRecord()
	{
		cout<<this->day<<"/"<<this->month<<"/"<<this->year<<endl;
	}
	void addNumberOfDays( int count )
	{
		this->day += count;
		if(this->month == 1 || this->month == 3 || this->month == 5 || this->month ==  7|| this->month == 8 ||this->month == 10 ||this->month == 12 )
		{
			if(this->day>31 && this->month ==12)
			{
				this->day -= 31;
				this->month -= 11;
				this->year++;
			}
			else if(this->day>31)
			{
				this->day -= 31;
				this->month++;
			}

		}
		else if(this->month == 2)
		{
			if(this->year%4 ==0 || this->year % 400==0)
			{
				if(this->day > 29)
				{
					this->day -= 29;
					this->month ++;
				}
			}
		}
		else
		{
			if(this->day >30)
			{
				this->day -= 30;
				this->month++;
			}
		}
	}
	string dayOfWeek()
	{
		switch(this->day%7)
		{
		case 0:
			return "Sunday";
		case 1:
			return "Monday";
		case 2:
			return "Tuesday";
		case 3:
			return "Wednesday";
		case 4:
			return "Thursday";
		case 5:
			return "Friday";
		case 6:
			return "Saturday";
		}
		return "";
	}
};

int menuList()
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Init"<<endl;
	cout<<"2.Accept Record"<<endl;
	cout<<"3.Print Record"<<endl;
	cout<<"4.Add number of days"<<endl;
	cout<<"5.Day of week"<<endl;
	cout<<"Enter choice"<<endl;
	cin>>choice;
	return choice;
}
void askCount(int &c)
{
	cout<<"Enter number of days u want to add:		"<<endl;
	cin>>c;
}
int main() {
	Date d;
	int choice;
	while((choice = menuList())!=0)
	{
		switch(choice)
		{
		case 1:
			d.init();
			break;
		case 2:
			d.acceptRecord();
			break;
		case 3:
			d.printRecord();
			break;
		case 4:
			int c;
			askCount(c);
			d.addNumberOfDays(c);
			break;
		case 5:
			string day = d.dayOfWeek();
			cout<<day<<endl;
			break;
		}
	}
	return 0;
}
