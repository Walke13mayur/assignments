============================================================================
#include<iostream>
#include<string>
#include "../includes/Node.h"
#include "../includes/Exception.h"
#include "../includes/LinkedList.h"

using namespace std;
using namespace kdac;


int main( void )
{
	try
	{
		LinkedList list;
		list.addLast( 10 );
		list.addLast( 20 );
		list.addLast( 30 );
		list.addFirst( 100);
		list.addAtPosition(80, 3);

		list.print( );
		list.removeFirst();
		list.print();

	}
	catch( Exception &ex )
	{
		cout<<ex.getMessage()<<endl;
	}
	return 0;
}
