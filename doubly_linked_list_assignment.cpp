#include <iostream>
using namespace std;

class Exception{
	string m;
public:
	Exception(string m = " "):m(m)
{}
	string getMessage()
	{
		return this->m;
	}
};
class Node{
	int data;
	Node *prev;
	Node *next;
public:
	Node(int data)
{
		this->data = data;
		this->prev = this->next = NULL;
}
 friend class DoublyLinkList;
 friend class Iterator;
};
class Iterator{
	Node *trav;
public:
	Iterator(Node *t)
{
		this->trav = t;
}
	bool operator!=(Iterator &o)
		{
			return this->trav != o.trav;
		}
	void operator++()
	{
		this->trav = this->trav->next;
	}
	void operator--()
		{
			this->trav =  this->trav->prev;
		}
	int operator*()
	{
		return this->trav->data;
	}

};
class DoublyLinkList{
	Node *head;
	Node *tail;
public:
	DoublyLinkList()
{
		this->head = this->tail = NULL;
}
	bool empty()
	{
		return this->head==NULL;
	}
	void addLast(int ele)
	{
		Node *temp  = new Node(ele);
		if(empty())
		{
			this->head = temp;
		}
		else
		{
			temp->prev = this->tail;
			this->tail->next = temp;
		}
		this->tail = temp;
	}
	void print()
	{
		Node *trav = this->head;
		while(trav!= NULL)
		{
			cout<<trav->data<<endl;
			trav = trav->next;
		}
	}
	Iterator frontbegin()
	{
		Iterator itr = this->head;
		return itr;
	}
	Iterator end()
	{
		Iterator itr = NULL;
		return itr;
	}
	Iterator rearBegin()
	{
		Iterator itr =  this->tail;
		return itr;
	}
	void removeFirst()
	{
		if(empty())
			throw Exception("Empty");
		Node *temp = this->head;
		this->head = this->head->next;
		this->head->prev = NULL;
		delete temp;
	}
	~DoublyLinkList()
	{
		while(this->empty())
		{
			this->removeFirst();
		}
	}
};
int main() {
	DoublyLinkList list;
	list.addLast(5);
	list.addLast(20);
	Iterator itrstart = list.rearBegin();
	Iterator itrend = list.end();
	while(itrstart != itrend)
	{
		cout<<(*itrstart)<<endl;
		--itrstart;
	}
	return 0;
}
