
#include<iostream>
using namespace std;

class Point
{
private:
	int xPos;
	int yPos;
public:
	Point(void)
	{
		this->xPos = 0;
		this->yPos = 0;
	}
	Point(const int x, const int y): xPos(x), yPos(y)
	{	}
	int getXPos() const
	{
		return xPos;
	}
	void setXPos(const int xPos)
	{
		this->xPos = xPos;
	}
	int getYPos() const
	{
		return yPos;
	}
	void setYPos(const int yPos)
	{
		this->yPos = yPos;
	}
};

int menu_list()
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Accept using setter"<<endl;
	cout<<"2.Display using getter"<<endl;
	cout<<"3.Modify using setter"<<endl;
	cout<<"Enter choice	:	"<<endl;
	cin>>choice;
	return choice;
}

int main(void)
{
	Point point;
	int choice;
	int xPos, yPos;
	while((choice = ::menu_list()) != 0)
	{
		switch(choice)
		{
		case 1:
			//Accept using setter
			cout<<"Enter xPos	:	";
			cin>>xPos;
			cout<<"Enter yPos	:	";
			cin>>yPos;
			point.setXPos(xPos);
			point.setYPos(yPos);
			break;
		case 2:
			//Display using getter
			xPos = point.getXPos();
			yPos = point.getYPos();
			cout<<"xPos	:	"<<xPos<<endl;
			cout<<"yPos	:	"<<yPos<<endl;
			break;
		case 3:
			//Modify using setter
			cout<<"Enter xPos to modify	:	";
			cin>>xPos;
			cout<<"Enter yPos to modify	:	";
			cin>>yPos;
			point.setXPos(xPos);
			point.setYPos(yPos);
			xPos = point.getXPos();
			yPos = point.getYPos();
			cout<<"Modified values are - "<<endl;
			cout<<"xPos	:	"<<xPos<<endl;
			cout<<"yPos	:	"<<yPos<<endl;
			break;
		}
	}
	return 0;
}


