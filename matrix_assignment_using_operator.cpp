#include<iostream>
using namespace std;

class DivideByZeroException {
private:
	string message;
public:
	DivideByZeroException(string message) :
			message(message) {
	}
	const string& getMessage() const {
		return message;
	}
};
class IllegalArgumentException {
private:
	string message;
public:
	IllegalArgumentException(string message) :
			message(message) {
	}
	const string& getMessage() const {
		return message;
	}
};

class Matrix {
private:
	int row;
	int col;
	int **matrix;
public:
	Matrix() :
			row(0), col(0), matrix(NULL) {
	}
	Matrix(int row, int col) {
		cout << "Matrix(int row, int col)" << endl;
		this->row = row;
		this->col = col;
		this->matrix = new int*[row];
		for (int row = 0; row < this->row; row++)
			this->matrix[row] = new int[this->col];
		cout << "Memory allocation successfull." << endl;
	}
	//const Matrix &other = m1
	//Matrix *const this = &m2
	Matrix(const Matrix &other) {
		this->row = other.row;
		this->col = other.col;
		this->matrix = new int*[this->row];
		cout << "Matrix(const Matrix &other)" << endl;
		for (int row = 0; row < this->row; row++) {
			this->matrix[row] = new int[this->col];
			for (int col = 0; col < this->col; col++) {
				this->matrix[row][col] = other.matrix[row][col];
			}
		}
	}
	void acceptRecord() throw (IllegalArgumentException) {
		int element;
		cout << "Enter elements for matrix	-\n";
		for (int row = 0; row < this->row; row++) {
			for (int col = 0; col < this->col; col++) {
				cout << "Enter element	:	";
				cin >> element;
				if (element == 0)
					throw IllegalArgumentException("Illegal argument exception");
				else
					this->matrix[row][col] = element;
			}
		}
	}
	void operator=(const Matrix &other)
	{
		for (int row = 0; row < this->row; row++) {
			for (int col = 0; col < this->col; col++) {
				this->matrix[row][col] = other.matrix[row][col];
			}
		}

	}
	Matrix operator+(Matrix &other) {
		Matrix m(2, 2);
		int res;
		for (int row = 0; row < this->row; row++) {
			for (int col = 0; col < this->col; col++) {
				res = this->matrix[row][col] + other.matrix[row][col];
				m.matrix[row][col] = res;
			}
		}
		return m;
	}
	Matrix operator-(Matrix &other) {
		Matrix m(2, 2);
		int res;
		cout << "Inside addition" << endl;
		for (int row = 0; row < this->row; row++) {
			for (int col = 0; col < this->col; col++) {
				res = this->matrix[row][col] - other.matrix[row][col];
				m.matrix[row][col] = res;
			}
		}
		return m;
	}
	Matrix operator*(Matrix &other) {
		Matrix m(2, 2);
		int res;
		cout << "Inside addition" << endl;
		for (int row = 0; row < this->row; row++) {
			for (int col = 0; col < this->col; col++) {
				res = this->matrix[row][col] * other.matrix[row][col];
				m.matrix[row][col] = res;
			}
		}
		return m;
	}
	Matrix operator/(Matrix &other) {
		Matrix m(2, 2);
		int res;
		cout << "Inside addition" << endl;
		for (int row = 0; row < this->row; row++) {
			for (int col = 0; col < this->col; col++) {
				res = this->matrix[row][col] / other.matrix[row][col];
				m.matrix[row][col] = res;
			}
		}
		return m;
	}
	void printRecord() {
		cout << "Matrix	:-	"<<endl;
		for (int row = 0; row < this->row; row++) {
			for (int col = 0; col < this->col; col++) {
				cout << this->matrix[row][col] << " ";
			}
			cout << endl;
		}
	}
	~Matrix() {
		cout<<"~Matrix()"<<endl;
		if (this->matrix != NULL) {
			for (int row = 0; row < this->row; row++) {
				delete[] this->matrix[row];
				this->matrix[row] = NULL;
			}
		}
		delete[] this->matrix;
		this->matrix = NULL;
		cout<<"Memory freed successfully." << endl;
	}
};

int menu_list() {
	int choice;
	cout<<"0.Exit" << endl;
	cout<<"1.Print matrix m1" << endl;
	cout<<"2.Print matrix m2" << endl;
	cout<<"3.Addition" << endl;
	cout<<"4.Subtraction" << endl;
	cout<<"5.Multiplication" << endl;
	cout<<"6.Division" << endl;
	cout<<"Enter choice : ";
	cin>>choice;
	return choice;
}

int main(void) {
	try {
		Matrix m1(2, 2);
		m1.acceptRecord();
		Matrix m2(2, 2);
		m2.acceptRecord();
		Matrix m3(2, 2);
		int choice;
		while ((choice = ::menu_list()) != 0) {
			try {
				switch (choice) {
				case 1:
					//print m1
					m1.printRecord();
					break;
				case 2:
					//print m2
					m2.printRecord();
					break;
				case 3:
					//Addition
					m3 = m1 + m2;	//m3 = m1.operator+(m2);
					m3.printRecord();
					break;
				case 4:
					//Subtraction
					m3 = m1 - m2;	//m3 = m1.operator-(m2);
					m3.printRecord();
					break;
				case 5:
					//Multiplication
					m3 = m1 * m2;	//m3 = m1.operator*(m2);
					m3.printRecord();
					break;
				case 6:
					//Division
					m3 = m1 / m2;	//m3 = m1.operator/(m2);
					m3.printRecord();
					break;
				default:
					cout << "Enter valid choice" << endl;
				}
			} catch (DivideByZeroException &ex) {
				cout << ex.getMessage() << endl;
			} catch (...) {
				cout << "Exception" << endl;
			}
		}
	} catch (IllegalArgumentException &ex) {
		cout << ex.getMessage() << endl;
	}
	return 0;
}
