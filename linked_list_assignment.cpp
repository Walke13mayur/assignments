

#include "../includes/Node.h"
#include "../includes/Exception.h"
#include "../includes/LinkedList.h"
using namespace kdac;
using namespace std;


LinkedList::LinkedList( void )throw( )
	{
		this->head = NULL;
		this->tail = NULL;
	}
	bool LinkedList:: empty( void )const throw( )
	{
		return this->head == NULL;
	}
	void LinkedList::addLast( int data )throw( bad_alloc )
	{
		Node *newNode = new Node( data );
		if( this->empty( ) )
			this->head = newNode;
		else
			this->tail->next = newNode;
		this->tail = newNode;
	}
	void LinkedList::addFirst( int data )throw( bad_alloc )
		{
			Node *newNode = new Node( data );
			if( this->empty( ) )
				this->head = newNode;
			else
				newNode->next = this->head;
			this->head = newNode;
		}
	void LinkedList:: addAtPosition( int data,int pos )throw( bad_alloc )
		{

			if( this->empty( ) || pos==1 )
				 addFirst(data);
			else
			{
				Node *newNode = new Node( data );
				Node *trav=this->head;
				for(int i=1;i<pos-1;i++)
				{
					trav=trav->next;
				}
				newNode->next=trav->next;
				trav->next=newNode;

			}

		}
	void LinkedList:: removeFirst( void )throw(Exception)
	{
		if( this->empty())
			throw Exception("LinkedList is empty");
		else if( this->head == this->tail)
		{
			delete this->head;
			this->head = this->tail = NULL;
		}
		else
		{
			Node *ptrNode = this->head;
			this->head = this->head->next;
			delete ptrNode;
		}
	}


	void LinkedList:: print( void )const throw( Exception )
	{
		if( this->empty() )
			throw Exception("LinkedList is empty");
		Node *trav = this->head;
		while( trav != NULL )
		{
			cout<<trav->data<<"	";
			trav = trav->next;
		}
		cout<<endl;
	}
	LinkedList ::~LinkedList( void )throw( )
	{
		while( !this->empty( ) )
			this->removeFirst();
	}
