
#include<iostream>
#include<string>
#include<cstring>
using namespace std;
class String
{
private:
	size_t length;
	char* buffer;
public:
	String() : length(0), buffer(NULL)
	{	}
	String(int length)
	{
		cout<<"String(int length)"<<endl;
		this->length = length;
		this->buffer = new char[length];
		cout<<"Memory allocation successful."<<endl;
	}
	String(const char* str)
	{
		long unsigned int i, length;
		length = strlen(str);
		cout<<"String(string str)"<<endl;
		this->length = length;
		this->buffer = new char[length];
		for(i = 0; i < length; i++)
			this->buffer[i] = str[i];
		cout<<"Memory allocation successful."<<endl;
	}
	String(const char* &Str)
	{
		int length = strlen(Str);
		this->length = length;
		this->buffer = new char[length];
		strcpy(buffer, Str);
	}
	void acceptString()
	{
		int size = this->length, i = 0;
		while(size)
		{
			cout<<"Enter character	:	";
			cin>>this->buffer[i];
			++i;
			--size;
		}
	}
	String operator+(const char* other)
	{
		strcat(this->buffer, other);
		this->length = this->length + strlen(other);
		return (*this);
	}
	void printString()
	{
		long unsigned int size = 0;
		cout<<"String	:	";
		while(size <= this->length)
		{
			cout<<this->buffer[size];
			++size;
		}
		cout<<endl;
	}
	~String()
	{
		cout<<"~String()"<<endl;
		delete[] this->buffer;
		this->buffer = NULL;
		if(this->buffer == NULL)
			cout<<"Memory freed successfully."<<endl;
	}
};
int main(void)
{
	String s1("Sunbeam ");
	String s2 = s1 + "Karad";	//s2 = s1.operator+("Karad");
	s2.printString();
	return 0;
}
